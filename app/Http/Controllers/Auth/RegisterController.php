<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use App\Models\Referido;
use App\Models\Model_has_role;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class RegisterController extends Controller
{

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\User
     */
    protected function create()
    {
        return view('register');
    }

     /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\User
     */
    protected function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'lastname' => 'required',
            'email' => 'required',
            'password' => 'required',
            'password_confirmation' => 'required|same:password',
        ]);
        
        
        $ref = Str::random(10);
        if ($request->refer != '') {
            //  si existe
            $referidor = User::where('referred',$request->refer)->get();
            if (json_encode($referidor) == '[]') {
                $route = 'register';
                $message = 'Referido no exite';
                return redirect($route)
                    ->with('error', $message);
            }
            $email = User::where('email',$request->email)->get();
            if (json_encode($email) != '[]') {
                $route = 'register';
                $message = 'El correo se encuentra en uso';
                return redirect($route)
                    ->with('error', $message);
            }
            $user = User::create([
                'referred' => $ref,
                'name' => $request->name,
                'lastname' => $request->lastname,
                'email' => $request->email,
                'password' => Hash::make($request->password),
            ]);
            $user_id = User::latest()->first()->id;
            Model_has_role::create(['role_id' => '2','model_type' => 'App\\Models\\User','model_id' => $user_id]);
            Referido::create([
                'id_user_ref' => $referidor[0]->id,
                'id_user' => $user_id,
            ]);

        }elseif ($request->refer == ''){
            // no existe
            $email = User::where('email',$request->email)->get();
            if (json_encode($email) != '[]') {
                $route = 'register';
                $message = 'El correo se encuentra en uso';
                return redirect($route)
                    ->with('error', $message);
            }
            $user = User::create([
                'referred' => $ref,
                'name' => $request->name,
                'lastname' => $request->lastname,
                'email' => $request->email,
                'password' => Hash::make($request->password),
            ]);
            $user_id = User::latest()->first()->id;
            Model_has_role::create(['role_id' => '2','model_type' => 'App\\Models\\User','model_id' => $user_id]);
            
        }

        auth()->guard()->login($user);

        return redirect('/home');
    }
}
