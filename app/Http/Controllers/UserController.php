<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\Gate;
use Brian2694\Toastr\Facades\Toastr;

class UserController extends Controller
{
    function __construct()
	{
		$this->middleware('auth');
		$this->middleware('permission:user-list', ['only' => ['index','store']]);
		$this->middleware('permission:user-create', ['only' => ['create','store']]);
		$this->middleware('permission:user-edit', ['only' => ['edit','update']]);
		$this->middleware('permission:user-delete', ['only' => ['destroy']]);

        $user_list = Permission::get()->filter(function($item) {
            return $item->name == 'user-list';
        })->first();
        $user_create = Permission::get()->filter(function($item) {
            return $item->name == 'user-create';
        })->first();
        $user_edit = Permission::get()->filter(function($item) {
            return $item->name == 'user-edit';
        })->first();
        $user_delete = Permission::get()->filter(function($item) {
            return $item->name == 'user-delete';
        })->first();


        if ($user_list == null) {
            Permission::create(['name'=>'user-list']);
        }
        if ($user_create == null) {
            Permission::create(['name'=>'user-create']);
        }
        if ($user_edit == null) {
            Permission::create(['name'=>'user-edit']);
        }
        if ($user_delete == null) {
            Permission::create(['name'=>'user-delete']);
        }
	}

	public function index(Request $request)
	{
		$users = User::all();
		return view('users.index',compact('users'));
	}

	public function create()
	{
		$permissions = Permission::get();
		return view('users.create',compact('permissions'));
	}

	public function store(Request $request)
	{
		$rules = [
            'name' 					=> 'required|unique:users,name',
            'code' 					=> 'required|unique:users,code',
			'permission' 			=> 'required',
        ];

        $messages = [
            'name.required'    		=> __('default.form.validation.name.required'),
            'name.unique'    		=> __('default.form.validation.name.unique'),
            'code.required'    		=> __('default.form.validation.code.required'),
            'code.unique'    		=> __('default.form.validation.code.unique'),
            'permission.required'   => __('default.form.validation.permission.required'),
        ];
        
        $this->validate($request, $rules, $messages);
       
		try {
            $user = User::create([
                'name' => $request->input('name'), 
                'code' => $request->input('code')
            ]);
			$user->syncPermissions($request->input('permission'));

            Toastr::success(__('user.message.store.success'));
		    return redirect()->route('users.index');
		} catch (Exception $e) {
            Toastr::error(__('user.message.store.error'));
		    return redirect()->route('users.index');
		} 
	}

	public function edit($id)
	{
        $user = User::find($id);
        $permissions = Permission::all();

		return view('users.edit',compact('user','permissions'));
	}

	public function update(Request $request, $id)
	{
		$rules = [
            'name' 					=> 'required|unique:users,name,' . $id,
            'code' 					=> 'required|unique:users,code,' . $id,
			'permission' 			=> 'required',
        ];

        $messages = [
            'name.required'    		=> __('default.form.validation.name.required'),
            'name.unique'    		=> __('default.form.validation.name.unique'),
            'code.required'    		=> __('default.form.validation.code.required'),
            'code.unique'    		=> __('default.form.validation.code.unique'),
            'permission.required'   => __('default.form.validation.permission.required'),
        ];
        
        $this->validate($request, $rules, $messages);

        try {
			$user = User::find($id);
			$user->name = $request->input('name');
			$user->code = $request->input('code');
			$user->save();
			$user->syncPermissions($request->input('permission'));

            Toastr::success(__('user.message.update.success'));
		    return redirect()->route('users.index');
		} catch (Exception $e) {
            Toastr::error(__('user.message.update.error'));
		    return redirect()->route('users.index');
		}
	}

	public function destroy()
	{
		$id = request()->input('id');
		$alluser = User::all();
		$countalluser = $alluser->count();

		if ($countalluser <= 1) {
			Toastr::error(__('user.message.warning_last_user'));
		    return redirect()->route('users.index');
		}else{
			$getuser = User::find($id);
			try {
				User::find($id)->delete();
				return back()->with(Toastr::error(__('user.message.destroy.success')));
			} catch (Exception $e) {
				$error_msg = Toastr::error(__('user.message.destroy.error'));
				return redirect()->route('users.index')->with($error_msg);
			}
		}
	}

}
