<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Plan;
use App\Models\Transaction;
use Illuminate\Http\Request;
use App\Http\Traits\UploadImage;
use Carbon\Carbon;
use App\Models\User;
use App\Models\Referido;
use App\Models\EventUser;
use App\Models\Retiros;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    use UploadImage;

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function landing()
    {
        $events = null;

        $plans = Plan::all();

        if(auth()->check()) {
            $events = EventUser::orderBy('id', 'desc')->where('user_id', auth()->user()->id)->whereDate('date', '=', date('Y-m-d'))->get();
        }

        //return json_encode($events);

        if ( json_encode($events) != '[]') {
            return view('landingu', compact('plans', 'events'));
        }
        if ( json_encode($events) == '[]' ) {
            return view('landing', compact('plans', 'events'));
        }        
    }

    public function nosotros()
    {
        return view('nosotros');
    }
    
    public function recargar()
    {
        return view('recargar');
    }

    public function retiro()
    {
        return view('retirar');
    }

    public function atencion_al_cliente()
    {
        return view('atencion_al_cliente');
    }

    public function index()
    {
        if(auth()->user()->complete == 1 || auth()->user()->complete == 0) {
            return redirect()->route('complete');
        }

        $planes = Plan::all();
        $user = User::find(Auth::id());
        return view('home', compact('planes','user'));
    }

    public function complete()
    {
        return view('complete');
    }

    public function completeProfile(Request $request)
    {
        $this->validate($request, [
            'qr' => 'mimes:jpeg,bmp,png,gif,svg,pdf',
            'network' => 'required',
            'wallet' => 'required',
        ]);

        $input = [
            'network' => $request->network,
            'wallet' => $request->wallet,
            'complete' => 2,
        ];

        if($request->hasFile('qr')) {
            $filename = $this->uploadImage($request->file('qr'), 'qrs');
            $input['qr'] = $filename;
        }

        auth()->user()->update($input);

        return redirect()->route('home');
    }

    public function plan($id)
    {
        if(auth()->user()->complete == 1 || auth()->user()->complete == 0) {
            return redirect()->route('complete');
        }

        $plan = Plan::findOrFail($id);
        return view('plan', compact('plan'));
    }

    public function store(Request $request, $id)
    {
        $this->validate($request, [
            'image' => 'required|mimes:jpeg,bmp,png,gif,svg,pdf',
            'amount' => 'required',
        ]);

        $filename = $this->uploadImage($request->file('image'), 'screenshots');

        Transaction::updateOrCreate(['user_id' => auth()->id()], [
            'amount' => $request->amount,
            'plan_id' => $id,
            'user_id' => auth()->id(),
            'screenshot' => $filename
        ]);

        return back()->with('status', 'Se envio la transacción, espere que el administrador la apruebe.');
    }


    public function transactions()
    {
        if (Auth::id() == 1) {
            $transactions = Transaction::all();
            return view('transactions', compact('transactions'));
        }else{
            $transactions = Transaction::where(['user_id' => Auth::id()])->get();
            $us = User::find(Auth::id());
            return view('transactions', compact('transactions','us'));
        }
        
    }

    public function mistransactions()
    {

        if (Auth::id() == 1) {
            $transactions = Transaction::all();
            return view('transactions', compact('transactions'));
        }else{
            
            $referidor = User::find(Auth::id());
            $referido = Referido::where('id_user_ref',$referidor->id)->get();
            $ref = [];
            $ganancia = 0;
            foreach ($referido as $value) {
                $ref[] = User::find($value->id_user);
                if ($referidor->plan_id == 1) {
                    $ganancia += Transaction::where('plan_id',1)->get()[0]->amount * 0.03;
                }
                elseif ($referidor->plan_id == 2) {
                    $ganancia += Transaction::where('plan_id',2)->get()[0]->amount * 0.05;   
                }
                elseif ($referidor->plan_id == 3) {
                    $ganancia += Transaction::where('plan_id',3)->get()[0]->amount * 0.07;
                }
            }
            $retiros = Retiros::where('user_id',Auth::id())->get();
            
            $retiro = 0;
            if ($retiros != '[]') {
                $retiro = $retiros[0]->status;
            }
            $transactions = Transaction::where('user_id' ,'=', Auth::id())
            ->where('updated_at', '>', date('Y-m-d', strtotime("+5 days")) )
            ->get();
            return view('mistransactions', compact('transactions','ganancia','retiros','retiro'));
        }
    }

    public function retirar(Request $request, $id)
    {
        $referidor = User::find(Auth::id());
        $cuenta = Transaction::find($id);
        $referido = Referido::where('id_user_ref',$cuenta->user_id)->get();
            $ganancia = 0;
            foreach ($referido as $value) {
                $ref[] = User::find($value->id_user);
                if ($referidor->plan_id == 1) {
                    $ganancia += Transaction::where('plan_id',1)->get()[0]->amount * 0.03;
                }
                elseif ($referidor->plan_id == 2) {
                    $ganancia += Transaction::where('plan_id',2)->get()[0]->amount * 0.05;   
                }
                elseif ($referidor->plan_id == 3) {
                    $ganancia += Transaction::where('plan_id',3)->get()[0]->amount * 0.07;
                }
            }

        Retiros::create([
            'plan_id' => $referidor->plan_id,
            'user_id' => Auth::id(),
            'amount_retiro' => $ganancia,
        ]);


        return back()->with('status', 'Se ha enviado su retiro con exito, espere que el administrador la apruebe.');
    }

    public function misreferidos()
    {

            $referidor = User::find(Auth::id());
            $referido = Referido::where('id_user_ref',$referidor->id)->get();
            $ref = [];
            foreach ($referido as $value) {
                $ref[] = User::find($value->id_user);
            }
            $ganancia = 0;
            if ($referidor->plan_id == 1) {
                $ganancia = Transaction::where('plan_id',1)->get()[0]->amount * 0.03;
            }
            elseif ($referidor->plan_id == 2) {
                $ganancia = Transaction::where('plan_id',2)->get()[0]->amount * 0.05;   
            }
            elseif ($referidor->plan_id == 3) {
                $ganancia = Transaction::where('plan_id',3)->get()[0]->amount * 0.07;
            }            

            return view('misreferidos', compact('ref','referidor','ganancia'));
        
    }

    public function activeTransaction($id)
    {
        $transaction = Transaction::find($id);

        $user = User::find($transaction->user_id);

        //$user->update(['plan_id' => $transaction->plan_id, 'complete' => 1]);
        $user->update(['plan_id' => $transaction->plan_id]);

        return back()->with('status', 'Usuario aprobado correctamente.');
    }

    public function click()
    {
        $event = EventUser::where('user_id', auth()->id())->whereDate('date', '=', date('Y-m-d'))->count();

        if($event == 5){
            return back()->with('status', 'No puedes hacer mas de 5 clicks por dia.');
        }

        $planId = auth()->user()->plan_id;

        EventUser::create([
            'user_id' => auth()->id(),
            'plan_id' => $planId,
            'click' => Carbon::now()->addMinutes($this->getClickByPlan($planId)),
            'date' => date('Y-m-d'),
        ]);

        return back()->with('status', 'Click realizado correctamente.');
    }

    public function getClickByPlan($planId)
    {
        $plan = Plan::find($planId);

        return $plan->time;
    }

    public function profile()
    {
        $user = User::find(auth()->id());

        return view('profile', compact('user'));
    }

    public function updateProfile(Request $request, $id)
    {
        $input = $request->all();

        $input['password'] = bcrypt($request->password);

        $this->validate($request, [
            'name' => 'required',
            'lastname' => 'required',
            'email' => 'required|email',
            'password' => 'confirmed',
        ]);

        $user = User::find($id);

        $user->update($input);

        return back()->with('status', 'Click realizado correctamente.');
    }

    public function events()
    {
        if (Auth::id() == 1) {
            $events = EventUser::with(['user', 'plan'])->get();
            return view('events', compact('events'));
        }else{
            $events = EventUser::where('user_id',Auth::id())->with(['user', 'plan'])->get();
            return view('events', compact('events'));
        }
        
    }
}
