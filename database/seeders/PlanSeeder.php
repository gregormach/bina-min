<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Plan;
use App\Models\Role_has_permission;
use App\Models\Role;
use App\Models\Model_has_role;
use App\Models\Permission;
use App\Models\User;
use Illuminate\Support\Str;

class PlanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Plan::create(['name' => 'Plan #1', 'price' => 20, 'price_max' => 99, 'time' => 120]);
        Plan::create(['name' => 'Plan #2', 'price' => 100, 'price_max' => 499, 'time' => 60]);
        Plan::create(['name' => 'Plan #3', 'price' => 500, 'price_max' => 999, 'time' => 20]);
        
        Role::create(['id' => '1','code' => 'admin','name' => 'Admin','guard_name' => 'web','created_at' => '2022-07-14 23:29:41','updated_at' => '2022-07-14 23:29:41']);
        Role::create(['id' => '2','code' => 'user','name' => 'User','guard_name' => 'web','created_at' => '2022-07-14 23:36:04','updated_at' => '2022-07-14 23:36:04']);

        Permission::create(['id' => '1','name' => 'user-list','guard_name' => 'web','created_at' => '2022-07-14 23:29:41','updated_at' => '2022-07-14 23:29:41']);
        Permission::create(['id' => '2','name' => 'user-create','guard_name' => 'web','created_at' => '2022-07-14 23:29:41','updated_at' => '2022-07-14 23:29:41']);
        Permission::create(['id' => '3','name' => 'user-edit','guard_name' => 'web','created_at' => '2022-07-14 23:29:41','updated_at' => '2022-07-14 23:29:41']);
        Permission::create(['id' => '4','name' => 'user-delete','guard_name' => 'web','created_at' => '2022-07-14 23:29:41','updated_at' => '2022-07-14 23:29:41']);
        Permission::create(['id' => '5','name' => 'profile-index','guard_name' => 'web','created_at' => '2022-07-14 23:29:42','updated_at' => '2022-07-14 23:29:42']);
        Permission::create(['id' => '6','name' => 'role-list','guard_name' => 'web','created_at' => '2022-07-14 23:29:42','updated_at' => '2022-07-14 23:29:42']);
        Permission::create(['id' => '7','name' => 'role-create','guard_name' => 'web','created_at' => '2022-07-14 23:29:42','updated_at' => '2022-07-14 23:29:42']);
        Permission::create(['id' => '8','name' => 'role-edit','guard_name' => 'web','created_at' => '2022-07-14 23:29:42','updated_at' => '2022-07-14 23:29:42']);
        Permission::create(['id' => '9','name' => 'role-delete','guard_name' => 'web','created_at' => '2022-07-14 23:29:42','updated_at' => '2022-07-14 23:29:42']);
        Permission::create(['id' => '10','name' => 'permission-list','guard_name' => 'web','created_at' => '2022-07-14 23:29:42','updated_at' => '2022-07-14 23:29:42']);
        Permission::create(['id' => '11','name' => 'permission-create','guard_name' => 'web','created_at' => '2022-07-14 23:29:42','updated_at' => '2022-07-14 23:29:42']);
        Permission::create(['id' => '12','name' => 'permission-edit','guard_name' => 'web','created_at' => '2022-07-14 23:29:42','updated_at' => '2022-07-14 23:29:42']);
        Permission::create(['id' => '13','name' => 'permission-delete','guard_name' => 'web','created_at' => '2022-07-14 23:29:42','updated_at' => '2022-07-14 23:29:42']);
        Permission::create(['id' => '22','name' => 'currency-list','guard_name' => 'web','created_at' => '2022-07-14 23:29:43','updated_at' => '2022-07-14 23:29:43']);
        Permission::create(['id' => '23','name' => 'currency-create','guard_name' => 'web','created_at' => '2022-07-14 23:29:43','updated_at' => '2022-07-14 23:29:43']);
        Permission::create(['id' => '24','name' => 'currency-edit','guard_name' => 'web','created_at' => '2022-07-14 23:29:43','updated_at' => '2022-07-14 23:29:43']);
        Permission::create(['id' => '25','name' => 'currency-delete','guard_name' => 'web','created_at' => '2022-07-14 23:29:43','updated_at' => '2022-07-14 23:29:43']);
        Permission::create(['id' => '26','name' => 'enlaces-list','guard_name' => 'web','created_at' => '2022-07-26 15:29:36','updated_at' => '2022-07-26 15:29:36']);
        Permission::create(['id' => '27','name' => 'content-perfil','guard_name' => 'web','created_at' => '2022-07-26 15:46:01','updated_at' => '2022-07-26 15:46:01']);
        Permission::create(['id' => '28','name' => 'content-perfil-user','guard_name' => 'web','created_at' => '2022-07-26 15:47:54','updated_at' => '2022-07-26 15:47:54']);
        
        Role_has_permission::create(['permission_id' => '1','role_id' => '1']);
        Role_has_permission::create(['permission_id' => '2','role_id' => '1']);
        Role_has_permission::create(['permission_id' => '3','role_id' => '1']);
        Role_has_permission::create(['permission_id' => '4','role_id' => '1']);
        Role_has_permission::create(['permission_id' => '5','role_id' => '1']);
        Role_has_permission::create(['permission_id' => '6','role_id' => '1']);
        Role_has_permission::create(['permission_id' => '7','role_id' => '1']);
        Role_has_permission::create(['permission_id' => '8','role_id' => '1']);
        Role_has_permission::create(['permission_id' => '9','role_id' => '1']);
        Role_has_permission::create(['permission_id' => '10','role_id' => '1']);
        Role_has_permission::create(['permission_id' => '11','role_id' => '1']);
        Role_has_permission::create(['permission_id' => '12','role_id' => '1']);
        Role_has_permission::create(['permission_id' => '13','role_id' => '1']);
        Role_has_permission::create(['permission_id' => '22','role_id' => '1']);
        Role_has_permission::create(['permission_id' => '23','role_id' => '1']);
        Role_has_permission::create(['permission_id' => '24','role_id' => '1']);
        Role_has_permission::create(['permission_id' => '25','role_id' => '1']);
        Role_has_permission::create(['permission_id' => '26','role_id' => '1']);
        Role_has_permission::create(['permission_id' => '27','role_id' => '1']);
        Role_has_permission::create(['permission_id' => '5','role_id' => '2']);
        Role_has_permission::create(['permission_id' => '26','role_id' => '2']);
        Role_has_permission::create(['permission_id' => '28','role_id' => '2']);


        Model_has_role::create(['role_id' => '1','model_type' => 'App\\Models\\User','model_id' => '1']);
        Model_has_role::create(['role_id' => '2','model_type' => 'App\\Models\\User','model_id' => '2']);
        
        User::create([
            'name' => 'User',
            'lastname' => 'User',
            'email' => 'user@minergatelite.dev',
            'password' => '$2y$10$Lp1fWAWugQmZ3NO5xr.RGO31xCYPMAUHelAVSw438gksvyR09dG1W', // password
            'remember_token' => Str::random(10)
            ]);
      



        
    }
}
