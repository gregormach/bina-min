<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\User>
 */
class UserFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'name' => 'Admin',
            'lastname' => 'Admin',
            'email' => 'admin@minergatelite.dev',
            'password' => '$2y$10$Lp1fWAWugQmZ3NO5xr.RGO31xCYPMAUHelAVSw438gksvyR09dG1W', // password
            'remember_token' => Str::random(10),
        ];
    }
}
