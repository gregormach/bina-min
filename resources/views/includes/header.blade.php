@if(auth()->user())
  
    <ul class="navbar-nav align-items-lg-center ml-lg-auto">
          <li class="nav-item">
            <a class="nav-link nav-link-icon" href="{{ url('nosotros') }}" title="{{ url('nosotros') }}">
              NOSOTROS
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link nav-link-icon" href="{{ url('recargar') }}" title="{{ url('recargar') }}">
              RECARGAR
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link nav-link-icon" href="{{ url('retiro') }}"  title="{{ url('retiro') }}">
              RETIRAR
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link nav-link-icon" href="{{ url('atencion_al_cliente') }}" title="{{ url('atencion_al_cliente') }}">
              SERVICIO AL CLIENTE
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link nav-link-icon" href="{{ route('home') }}" title="{{ route('home') }}">
              DASHBOARD
            </a>
          </li>
          {{-- <li class="nav-item">
            <a class="nav-link nav-link-icon" href="{{ url('logout') }}" title="{{ url('logout') }}">
              LOGOUT
            </a>
          </li> --}}
          <li class="nav-item">
            <a class="nav-link nav-link-icon" href="#"  data-toggle="tooltip" title="Facebook">
              <i class="fa fa-facebook-square"></i>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link nav-link-icon" href="#"  data-toggle="tooltip" title="Instagram">
              <i class="fa fa-instagram"></i>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link nav-link-icon" href="#"  data-toggle="tooltip" title="Twitter">
              <i class="fa fa-twitter-square"></i>
            </a>
          </li>
        </ul>

    @else
        <ul class="navbar-nav align-items-lg-center ml-lg-auto">
          <li class="nav-item">
            <a class="nav-link nav-link-icon" href="{{ url('nosotros') }}" title="{{ url('nosotros') }}">
              NOSOTROS
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link nav-link-icon" href="{{ url('recargar') }}" title="{{ url('recargar') }}">
              RECARGAR
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link nav-link-icon" href="{{ url('retiro') }}"  title="{{ url('retiro') }}">
              RETIRAR
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link nav-link-icon" href="{{ url('atencion_al_cliente') }}" title="{{ url('atencion_al_cliente') }}">
              SERVICIO AL CLIENTE
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link nav-link-icon" href="{{ url('register') }}" title="{{ url('register') }}">
              REGISTRESE
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link nav-link-icon" href="{{ url('login') }}" title="{{ url('login') }}">
              LOGIN
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link nav-link-icon" href="#"  data-toggle="tooltip" title="Facebook">
              <i class="fa fa-facebook-square"></i>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link nav-link-icon" href="#"  data-toggle="tooltip" title="Instagram">
              <i class="fa fa-instagram"></i>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link nav-link-icon" href="#"  data-toggle="tooltip" title="Twitter">
              <i class="fa fa-twitter-square"></i>
            </a>
          </li>
        </ul>


@endif