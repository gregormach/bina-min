<nav id="sidebar" aria-label="Main Navigation">
    <div class="bg-header-dark">
        <div class="content-header bg-white-5">
            <a class="fw-semibold text-white tracking-wide" href="./">
                <span class="smini-visible">
                    {{ config('app.name') }}
                </span>
                <span class="smini-hidden">
                    {{ config('app.name') }}
                </span>
            </a>
        </div>
    </div>
    <div class="js-sidebar-scroll">
        <div class="content-side">
            <ul class="nav-main">
                <li class="nav-main-item">
                    <a class="nav-main-link" href="{{ route('home') }}">
                        <i class="nav-main-link-icon fa fa-home"></i>
                        <span class="nav-main-link-name">Dashboard</span>
                    </a>
                </li>

                @if(auth()->user()->can('enlaces-list'))

                    @if(auth()->user()->can('user-list'))
                        <li class="nav-main-item">
                            <a class="nav-main-link" href="{{ route('transactions') }}">
                                <i class="nav-main-link-icon fa fa-building"></i>
                                <span class="nav-main-link-name">Usuarios con planes</span>
                            </a>
                           </li>
                        <li class="nav-main-item">
                            <a class="nav-main-link" href="{{ route('profile') }}">
                                <i class="nav-main-link-icon fa fa-user"></i>
                                <span class="nav-main-link-name">Profile</span>
                            </a>
                        </li>
                        <li class="nav-main-item">
                            <a class="nav-main-link" href="{{ route('events') }}">
                                <i class="nav-main-link-icon fa fa-users"></i>
                                <span class="nav-main-link-name">Eventos</span>
                            </a>
                        </li>
                        @else
                        <li class="nav-main-item">
                            <a class="nav-main-link" href="{{ route('transactions') }}">
                                <i class="nav-main-link-icon fa fa-building"></i>
                                <span class="nav-main-link-name">Mi plan</span>
                            </a>
                           </li>
                        <li class="nav-main-item">
                            <a class="nav-main-link" href="{{ route('mistransactions') }}">
                                <i class="nav-main-link-icon fa fa-building"></i>
                                <span class="nav-main-link-name">Mis transacciones</span>
                            </a>
                           </li>
                        <li class="nav-main-item">
                            <a class="nav-main-link" href="{{ route('misreferidos') }}">
                                <i class="nav-main-link-icon fa fa-building"></i>
                                <span class="nav-main-link-name">Mis referidos</span>
                            </a>
                           </li>
                        <li class="nav-main-item">
                            <a class="nav-main-link" href="{{ route('profile') }}">
                                <i class="nav-main-link-icon fa fa-user"></i>
                                <span class="nav-main-link-name">Profile</span>
                            </a>
                        </li>
                        <li class="nav-main-item">
                            <a class="nav-main-link" href="{{ route('events') }}">
                                <i class="nav-main-link-icon fa fa-users"></i>
                                <span class="nav-main-link-name">Eventos</span>
                            </a>
                        </li>
                           
                    @endif
                   
                   
                   
                    
                @endif

                <!-- Users -->
                @if(auth()->user()->can('user-list') || auth()->user()->can('role-list') || auth()->user()->can('permission-list'))
                           @can('user-list')
                                <li class="nav-main-item">
                                    <a href="{{ route('users.index') }}" title="Usuarios" class="nav-main-link">
                                        <i class="nav-main-link-icon fa fa-users"></i>
                                        <span class="nav-main-link-name">Usuarios</span>
                                    </a>
                                </li>
                            @endcan

                            @can('role-list')
                                <li class="nav-main-item">
                                    <a href="{{ route('roles.index') }}" title="Roles" class="nav-main-link">
                                        <i class="nav-main-link-icon fa fa-users"></i>
                                        <span class="nav-main-link-name">Roles</span>
                                    </a>
                                </li>
                            @endcan

                            @can('permission-list')
                                <li class="nav-main-item">
                                    <a href="{{ route('permissions.index') }}" title="Permisos" class="nav-main-link">
                                        <i class="nav-main-link-icon fa fa-users"></i>
                                        <span class="nav-main-link-name">Permisos</span>
                                    </a>
                                </li>
                            @endcan
                @endif

                
            </ul>
        </div>
    </div>
</nav>
