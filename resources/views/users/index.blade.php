@extends('layouts.app')
@section('content')

<div class="content">
    @if (session('status'))
    <div class="alert alert-success" role="alert">
        {{ session('status') }}
    </div>
    @endif
    <div class="block block-rounded">
      <div class="block-header block-header-default">
        <h3 class="block-title">Usuarios</h3>
      </div>
      <div class="block-content">
      	{{-- @if (Gate::check('user-create'))
					<div class="col-md-3">
						<div class="create-btn pull-right">
							<a href="{{ route('users.create') }}" class="btn custom-create-btn"></a>
						</div>
					</div>
				@endif  --}}
        <div class="table-responsive">
          <table class="table table-bordered table-striped table-vcenter">
            <thead>
							<tr>
								<th>#</th>
								<th>Nombre</th>
								<th>Correo</th>
								<th>Wallet</th>

								{{-- @if(Gate::check('user-edit') || Gate::check('user-delete'))
									<th>Acción</th>
								@endif  --}}
							</tr>
						</thead>
            <tbody>

            	@foreach($users as $user)
								<tr>
									<td  class="text-center">{{$user->id}}</td>
									<td  class="text-center">{{$user->name}}</td>
									<td  class="text-center">{{$user->email}}</td>
									<td  class="text-center">{{$user->wallet}}</td>

									{{-- <td>
										@if(Gate::check('user-edit'))
											<a href="{{route('users.edit', $user->id)}}" class="custom-edit-btn mr-1">
												<i class="fe fe-pencil"></i>
													Editar
											</a>
										@endif 

										@if( Gate::check('user-delete'))
											<span class="flex justify-center items-center">
												<button class="custom-delete-btn remove-user" data-id="{{ $user->id }}" data-action="/users/destroy">
													<i class="fe fe-trash"></i>
													Borrar
												</button>
											</span>
										@endif 
									</td> --}}
								</tr>
							@endforeach
               
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <!-- END Full Table -->
  </div>
@endsection




@push('scripts')
<script>
	$(document).ready( function () {
		$('#user_table').DataTable({
			language: {
                    "decimal": ",",
                    "thousands": ".",
                    "info": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "infoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "infoPostFix": "",
                    "infoFiltered": "(filtrado de un total de _MAX_ registros)",
                    "loadingRecords": "Cargando...",
                    "lengthMenu": "Mostrar _MENU_ registros",
                    "paginate": {
                        "first": "Primero",
                        "last": "Último",
                        "next": "Siguiente",
                        "previous": "Anterior"
                    },
                    "processing": "Procesando...",
                    "search": "Buscar:",
                    "searchPlaceholder": "",
                    "zeroRecords": "No se encontraron resultados",
                    "emptyTable": "Ningún dato disponible en esta tabla",
                    "aria": {
                        "sortAscending":  ": Activar para ordenar la columna de manera ascendente",
                        "sortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                    
                }
		});
	} );
</script>

<script type="text/javascript">
	$("body").on("click",".remove-user",function(){
		var current_object = $(this);
		swal({
			title: "Esta seguro?",
			text: "You will not be able to recover this data!",
			type: "error",
			showCancelButton: true,
			dangerMode: true,
			cancelButtonClass: '#DD6B55',
			confirmButtonColor: '#dc3545',
			confirmButtonText: 'Delete!',
		},function (result) {
			if (result) {
				var action = current_object.attr('data-action');
				var token = jQuery('meta[name="csrf-token"]').attr('content');
				var id = current_object.attr('data-id');

				$('body').html("<form class='form-inline remove-form' method='POST' action='"+action+"'></form>");
				$('body').find('.remove-form').append('<input name="_method" type="hidden" value="post">');
				$('body').find('.remove-form').append('<input name="_token" type="hidden" value="'+token+'">');
				$('body').find('.remove-form').append('<input name="id" type="hidden" value="'+id+'">');
				$('body').find('.remove-form').submit();
			}
		});
	});
</script>
@endpush