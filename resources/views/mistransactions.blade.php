@extends('layouts.app')

@section('content')
<div class="content">
    @if (session('status'))
    <div class="alert alert-success" role="alert">
        {{ session('status') }}
    </div>
    @endif
    <div class="block block-rounded">
      <div class="block-header block-header-default">
        <h3 class="block-title">Mis Transacciones</h3>
      </div>
      <div class="block-content">
        <div class="table-responsive">
          <table class="table table-bordered table-striped table-vcenter">
            <thead>
              <tr>
                <th class="text-center">ID</th>
                <th class="text-center">Plan</th>
                <th class="text-center">Usuario</th>
                <th class="text-center" style="width: 15%;">Comprobante</th>
                <th class="text-center">Estado</th>
                <th class="text-center">Ganancias</th>
                <th class="text-center">Opción</th>
              </tr>
            </thead>
            <tbody>
                @foreach ($transactions as $item)
                <tr>
                    <td class="text-center">
                        {{ $item->id }}
                      </td>

                    <td class="text-center">
                      #{{ $item->plan_id }}
                    </td>

                    <td class="text-center">
                        {{ $item->user->name }}
                    </td>

                    <td class="text-center">
                        <a href="{{ url('storage/screenshots', $item->screenshot) }}" target="_blank">
                            <img src="{{ url('storage/screenshots', $item->screenshot) }}" style="width: 50%">
                        </a>
                    </td>

                    <td class="text-center">
                        {{ $item->user->complete ? 'Completo' : 'Pendiente' }}
                    </td>
                    <td class="text-center">
                        {{ $ganancia }}
                    </td>

                    <td class="text-center">
                      <div class="btn-group">
                        @if(auth()->user()->can('content-perfil'))
                           @if (!$item->user->complete == 1)
                            <form action="{{ route('transaction', $item->id) }}" method="POST">
                                @csrf
                                <button type="submit" class="btn btn-sm btn-alt-secondary js-bs-tooltip-enabled" data-bs-toggle="tooltip" title="" data-bs-original-title="Edit">
                                    <i class="fa fa-check"></i>
                                </button>
                            </form>
                            @endif
                            <button type="button" class="btn btn-sm btn-alt-secondary js-bs-tooltip-enabled" data-bs-toggle="tooltip" title="" data-bs-original-title="Delete">
                              <i class="fa fa-times"></i>
                            </button>
                        @endif
                        @if(auth()->user()->can('content-perfil-user'))
                            @if ($item->user->complete == 1  && $retiros == '[]' )
                                <i class="fa fa-check"></i> Por favor complete su perfil.
                            @elseif ($item->user->complete == 2 && $retiros == '[]' )

                                <form action="{{ route('retirar', $item->id) }}" method="POST">
                                    @csrf
                                    <button type="submit" class="btn btn-success" data-bs-toggle="tooltip" title="" data-bs-original-title="Retirar">
                                        Retirar
                                    </button>
                                </form>
                                @else
                                    @if ($retiro == 1)
                                      APROBADO
                                    @elseif ($retiro == 0)
                                      EN ESPERA                                         
                                    @endif
                            @endif
                        @endif                        
                      </div>
                    </td>
                  </tr>
                @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <!-- END Full Table -->
  </div>
@endsection



