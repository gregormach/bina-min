<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="apple-touch-icon" sizes="76x76" href="/web/assets/img/apple-icon.png">
  <link rel="icon" type="image/png" href="/web/assets/img/favicon.png">
  <title>
    {{ config('app.name') }}
  </title>
  <!--     Fonts and icons     -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
  <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
  <!-- Nucleo Icons -->
  <link href="/web/assets/css/nucleo-icons.css" rel="stylesheet" />
  <link href="/web/assets/css/nucleo-svg.css" rel="stylesheet" />
  <!-- Font Awesome Icons -->
  <link href="/web/assets/css/font-awesome.css" rel="stylesheet" />
  <link href="/web/assets/css/nucleo-svg.css" rel="stylesheet" />
  <!-- CSS Files -->
  <link href="/web/assets/css/argon-design-system.css?v=1.2.2" rel="stylesheet" />
</head>

<body class="landing-page">
  <!-- Navbar -->
  <nav id="navbar-main" class="navbar navbar-main navbar-expand-lg navbar-transparent navbar-light py-2">
    <div class="container">
      <a class="navbar-brand mr-lg-5" href="/">
        <img src="{{ asset('/web/assets/img/logo.png') }}">
      </a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar_global" aria-controls="navbar_global" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="navbar-collapse collapse" id="navbar_global">
        <div class="navbar-collapse-header">
          <div class="row">
            <div class="col-6 collapse-brand">
              <a href="/">
                <img src="{{ asset('/web/assets/img/logo.png') }}">
              </a>
            </div>
            <div class="col-6 collapse-close">
              <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbar_global" aria-controls="navbar_global" aria-expanded="false" aria-label="Toggle navigation">
                <span></span>
                <span></span>
              </button>
            </div>
          </div>
        </div>
        @include('includes.header')
      </div>
    </div>
  </nav>
  <!-- End Navbar -->
  <div class="wrapper">
    <div class="section section-hero section-shaped">
      <div class="shape shape-style-3 shape-default">
        <span class="span-150"></span>
        <span class="span-50"></span>
        <span class="span-50"></span>
        <span class="span-75"></span>
        <span class="span-100"></span>
        <span class="span-75"></span>
        <span class="span-50"></span>
        <span class="span-100"></span>
        <span class="span-50"></span>
        <span class="span-100"></span>
      </div>
      <div class="page-header">
        <div class="container shape-container d-flex align-items-center py-lg">
          <div class="col px-0">
            <div class="row align-items-center justify-content-center">
              @auth
              <div class="col-lg-10 text-center">
                    <h1 class="text-white display-1">
                        Click Disponibles ({{ (5 - $events->count()) }})
                    </h1>
                    @if ($events->count() > 0)
                    <h2 class="display-4 font-weight-normal text-white">
                        Proximo click {{ Carbon\Carbon::parse($events->first()->click)->diffForHumans()  }}
                    </h2>
                    <div class="btn-wrapper mt-4">
                        <img src="/web/assets/img/datlas_bitcoin_header2.gif" class="img-fluid">
                    </div>
                    @endif
                    @if (Carbon\Carbon::now()->gte($events->first()->click))
                        <div class="mt-4">
                            <a href="{{ route('click') }}" class="btn btn-default">
                                Click Aqui
                            </a>
                        </div>
                    @endif
                </div>
              @else
              <div class="col-lg-10 text-center">
                    <h1 class="text-white display-1">
                        {{ config('app.name') }}
                    </h1>
                    <a href="{{ route('login') }}">
                        <h2 class="display-4 font-weight-normal text-white">
                            Login
                        </h2>
                    </a>
                </div>
              @endauth
            </div>
          </div>
        </div>
      </div>
      <div class="separator separator-bottom separator-skew zindex-100">
        <svg x="0" y="0" viewBox="0 0 2560 100" preserveAspectRatio="none" version="1.1" xmlns="http://www.w3.org/2000/svg">
          <polygon class="fill-white" points="2560 0 2560 100 0 100"></polygon>
        </svg>
      </div>
    </div>

    <div class="section features-1">
      <div class="container">
        <div class="row">
          <div class="col-md-8 mx-auto text-center">
            <h3 class="display-3">ATENCIÓN AL CLIENTE</h3>
            <p class="lead"></p>
          </div>
        </div>
        <div class="row" style="text-align: justify;  padding-left: 15px; padding-right: 15px;" >
          <h3>CONDICIONES DE USO:</h3>
        </div>
        <div class="row" style="text-align: justify; padding-left: 15px; padding-right: 15px;" >
          <p>Si desea utilizar nuestra Plataforma, además declara y garantiza que:</p>
          <p>1. Tiene al menos 18 años o es mayor de edad, según su jurisdicción pertinente.</p>
          <p>2. No han sido suspendidos o eliminados previamente de nuestra Plataforma.</p>
          <p>3. tener pleno poder y autoridad para entrar en esta relación legal y al hacerlo no violará ninguna otra relación legal.</p>
          <p>4. usar nuestra Plataforma con su propio correo electrónico y para su propio beneficio y no actuar en nombre y/o en interés de ninguna otra persona.</p>
          <p>5. utilizará su equipo y poder de procesamiento bajo su propio riesgo. MinerGate Lite no es responsable de ningún problema técnico que pueda enfrentar al usar su equipo y poder de procesamiento en nuestra Plataforma.</p>
          <p>6. no usará nuestra Plataforma o dejará de usarla inmediatamente si alguna ley aplicable en su país lo prohíbe o se lo prohibirá en cualquier momento.</p>
          <p>7. no usará la Plataforma o dejará de usarla inmediatamente si es residente o se convierte en residente en cualquier momento de un estado o región (de acuerdo con la definición de residencia de dicho estado o región), donde los retiros de criptoactivos que va a realizar ejecutar están prohibidos o requieren aprobación especial, permiso y/o autorización de cualquier tipo, que MinerGate Lite no haya obtenido en este estado o región.</p>
        </div>
        <div class="row" style="text-align: justify;  padding-left: 15px; padding-right: 15px;" >
          <h3>TÉRMINOS DE SERVICIO:</h3>
        </div>
        <div class="row" style="text-align: justify;  padding-left: 15px; padding-right: 15px;" >
          <p>Los siguientes Términos de SERVICIO y cualquier término incorporado aquí, constituyen un acuerdo legalmente vinculante entre usted "Usuario" y "Nosotros" (“MinerGate Lite"), con respecto a su acceso a y el uso de nuestra "Plataforma", incluido la tecnología y la plataforma integrada en el mismo, cualquier aplicación asociada con el mismo y cualquier otra forma de medios, canal de medios, sitio web móvil o aplicación móvil relacionada, vinculado o conectado de otro modo a la Plataforma.</p>
          <p>Usted acepta que al acceder a la Plataforma, ha leído, entendido y aceptado estar sujeto a todos estos Términos de uso. Si no está de acuerdo con los Términos, entonces tiene prohibido usar la Plataforma y debe dejar de usarla de inmediato.</p>
          <p>MinerGate Lite es una plataforma que combina varios grupos de minería que le permite extraer diferentes tipos de criptoactivos.</p>
        </div>  
        <div class="row" style="text-align: justify;  padding-left: 15px; padding-right: 15px;" >
          <h3>Minería de criptoactivos</h3>
        </div>
        <div class="row" style="text-align: justify;  padding-left: 15px; padding-right: 15px;" >
          <p>Nuestra plataforma permite a los usuarios conectar su poder de procesamiento en un grupo de minería con el fin de obtener criptoactivos. Para utilizar nuestra Plataforma, el usuario deberá registrarse en nuestro Sitio Web. Para obtener más información acerca de las reglas y procedimientos del proceso de minería, deberá leer y analizar atentamente los manuales respectivos en nuestro sitio web.</p>
            
          <p>En nuestro sitio web puede encontrar información adicional sobre la minería de criptoactivos, a saber:
          <p>·         guías paso a paso sobre cómo trabajar con la Plataforma en diferentes sistemas operativos.</p>
          <p>·         instrucciones sobre cómo comenzar a minar criptoactivos en mineros en la nube.</p>
          <p>·         si tiene más preguntas sobre el proceso de minería de criptoactivos, puede contactarnos en nuestra página web.</p></p>

          <p>En lo que respecta a compartir su poder de procesamiento con otros usuarios, MinerGate Lite no puede garantizar que obtendrá una cantidad particular de criptoactivos en un período de tiempo particular. Es por eso que usted INDEMNIZA Y EXONERA A MINERGATE DE CUALQUIER RECLAMO, DEMANDA Y DAÑO, YA SEA DIRECTO, INDIRECTO, CONSECUENTE O ESPECIAL, O CUALQUIER OTRO DAÑO DE CUALQUIER TIPO, INCLUYENDO, ENTRE OTROS, PÉRDIDA DE USO, PÉRDIDA DE BENEFICIOS O PÉRDIDA DE DATOS, YA SEA EN UNA ACCIÓN POR CONTRATO, AGRAVIO (INCLUYENDO, ENTRE OTROS, LA NEGLIGENCIA) O DE CUALQUIER CUALQUIER ORIGEN, O DE CUALQUIER MANERA RELACIONADA CON LA MINERÍA DE CRIPTOACTIVOS EN NUESTRA PLATAFORMA.</p>
          </div>  
          <div class="row" style="text-align: justify;  padding-left: 15px; padding-right: 15px;" >
            <h3>TERMINOLOGIA</h3>
          </div>
          <div class="row" style="text-align: justify;  padding-left: 15px; padding-right: 15px;" >
          <p>1.    " Cuenta " es una cuenta de Usuario, accesible después del proceso de registro, donde los activos criptográficos pueden almacenarse después del proceso de extracción y ser retirados por el usuario al servicio de terceros.</p>    
          <p>2.    " Criptoactivos " significará, para los fines del presente, ese tipo de activos que pueden transmitirse única y exclusivamente por medio de tecnología de cadena de bloques, incluidos, entre otros, monedas digitales y tokens digitales y cualquier otro tipo de medio digital de intercambio, como como Bitcoin, Ethereum o Ripple.</p>  
          <p>3.    " Evento de Fuerza Mayor " se entenderá como cualquier evento fuera del control razonable de MinerGate Lite, incluyendo pero no limitado a una inundación, condiciones climáticas extraordinarias, terremoto u otro caso fortuito, incendio, guerra, insurrección, motín, conflicto laboral, accidente, acción de gobierno, suspensión de cuentas bancarias de cualquier tipo, saltos extraordinarios en el valor de los criptoactivos; fallas en las comunicaciones, la red o el suministro eléctrico; o mal funcionamiento del equipo o software o cualquier otra causa más allá del control razonable de MinerGate Lite.</p>
          <p>4.    " Minería " se referirá a los fines del presente documento a un proceso en el que las transacciones de varios tipos de criptoactivos se verifican y se agregan al libro de contabilidad digital de la cadena de bloques, lo que resulta en una cierta cantidad de criptoactivos que se distribuyen entre los propietarios del poder de procesamiento que participan en dicho un proceso.</p>
          <p>5.    " Grupo " significará, a los efectos del presente, la agrupación (fusión) de recursos por parte de los usuarios, que conectan su poder de procesamiento con la ayuda de la Plataforma, para dividir los criptoactivos en partes iguales, de acuerdo con la cantidad de poder de procesamiento que contribuyeron a la minería. proceso.</p>
          <p>6.    " Servicio de terceros " es cualquier plataforma o red en la que los criptoactivos le pertenecen o donde usted es el beneficiario final de los criptoactivos; y esta plataforma es mantenida por un tercero fuera de la Plataforma; incluyendo, pero no limitado a cuentas de terceros.</p>
          <p>7.    " Retiro " para los fines del presente documento significará una transacción de criptoactivos fuera de la Cuenta de un Usuario, que MinerGate Lite ejecuta técnicamente de acuerdo con la solicitud del Usuario.</p>
          </div>  
          <div class="row" style="text-align: justify;  padding-left: 15px; padding-right: 15px;" >
            <h3>REGISTRO WEB</h3>
          </div>
          <div class="row" style="text-align: justify;  padding-left: 15px; padding-right: 15px;" >
          <p>1.    Es necesario pasar por el proceso de registro y crear una cuenta con MinerGate Lite para utilizar algunas funciones de nuestro sitio web, tal como se prescribe en este documento.<br>
          2.    MinerGate Lite se reserva el derecho, a su exclusivo criterio, de limitar la cantidad de Cuentas que puede tener, mantener o crear. Las cuentas no se pueden asignar a ningún tercero.
          3.    Cuando crea una Cuenta, se compromete a: cree una contraseña segura que no use para otros sitios web, servicios en línea o fuera de línea; proporcionar información precisa y veraz. Consulte nuestra Politica de privacidad sobre cómo recopilamos, usamos y compartimos su información personal; aceptar pasar por nuestros procedimientos de seguridad internos, que pueden aplicarse a usted de vez en cuando;
          ·         mantener y actualizar rápidamente la información de su Cuenta;<br>
          ·         mantener la seguridad de su Cuenta protegiendo su contraseña y restringiendo el acceso a su Cuenta;<br>
          ·         Notificarnos de inmediato si descubre o sospecha cualquier violación de seguridad relacionada con su Cuenta;
          asumir la responsabilidad de todas las actividades que ocurran en su Cuenta y aceptar todos los riesgos de cualquier acceso autorizado o no autorizado a su Cuenta, en la máxima medida permitida por la ley.</p>
          </div>  
          <div class="row" style="text-align: justify;  padding-left: 15px; padding-right: 15px;" >
            <h3>DECLARACION JURADA</h3>
          </div>
          <div class="row" style="text-align: justify;  padding-left: 15px; padding-right: 15px;" >

          <p>Al acceder o utilizar la Plataforma, usted declara, acepta y garantiza que no infringirá ninguna ley, contrato, propiedad intelectual u otro derecho de terceros ni cometerá un agravio, y que es el único responsable de su conducta mientras utiliza nuestra Plataforma. Sin perjuicio de la generalidad de lo anterior, usted declara, acepta y garantiza que no:
          1.    Usar la Plataforma de cualquier manera que pueda interferir, interrumpir, afectar negativamente o impedir que otros usuarios usen la Plataforma con todas las funciones, o que pueda dañar, deshabilitar, sobrecargar o perjudicar el funcionamiento de la Plataforma de cualquier manera;<br>
          2.    Usar la Plataforma para pagar, apoyar o participar de cualquier otro modo en cualquier actividad de juego ilegal; fraude; lavado de dinero; o actividades terroristas; o cualquier otra actividad ilegal;<br>
          3.    Usar cualquier robot, araña, rastreador, raspador u otro medio o interfaz automatizado no proporcionado por Nosotros para acceder a la Plataforma o para extraer datos;<br>
          4.    Vender, alquilar, arrendar, otorgar licencias, sublicenciar, transferir, distribuir, retransmitir, compartir en tiempo, utilizar como oficina de servicios o de otro modo ceder a terceros los servicios de nuestro sitio web o cualquiera de sus derechos de acceso y uso de servicios como otorgado específicamente por los Términos;<br>
          5.    Usar o intentar usar otra Cuenta de usuario sin autorización;<br>
          6.    Eliminar cualquier aviso de derechos de autor u otros avisos de propiedad de nuestro sitio web;<br>
          7.    Intentar eludir cualquier técnica de filtrado de contenido que empleemos, o intentar acceder a cualquier servicio o área de nuestra Plataforma tecnológica a la que no esté autorizado a acceder;<br>
          8.    Cargar o intentar cargar archivos que contengan virus, caballos de Troya, gusanos, bombas de tiempo, cancelbots, archivos corruptos o cualquier otro software o programa similar que pueda dañar el funcionamiento de la propiedad de otra persona;<br>
          9.    Proporcionar información falsa, inexacta o engañosa;<br>
          10.  Alentar o inducir a cualquier tercero a participar en cualquiera de las actividades prohibidas en esta Sección.</p>
          </div>  
          <div class="row" style="text-align: justify;  padding-left: 15px; padding-right: 15px;" >
            <h3>DIVULGACIÓN DE RIESGO</h3>
          </div>
          <div class="row" style="text-align: justify;  padding-left: 15px; padding-right: 15px;" >

          <p>Debido a nuestras políticas internas, solo brindamos servicios a usuarios con suficiente experiencia, conocimiento y comprensión de los principios de funcionamiento de nuestra Plataforma, y aquellos que comprenden completamente los riesgos asociados. Usted reconoce y acepta que accederá y utilizará la Plataforma bajo su propio riesgo. Usted reconoce y acepta la posibilidad de los riesgos, indicados en esta cláusula de los Términos.<br>
          Riesgo de piratería y debilidades de seguridad. Los piratas informáticos u otros grupos u organizaciones maliciosos pueden intentar interferir con la Plataforma de diversas formas, incluidos, entre otros, ataques de malware, ataques DoS, ataques Sybil, smurfing y spoofing y cualquier otro tipo de actividad maliciosa.<br>
          Riesgos asociados a la regulación legal. Las tecnologías de cadena de bloques han sido objeto de escrutinio por parte de varios organismos reguladores de todo el mundo, él funcionamiento del sitio web y la plataforma podría verse afectado por una o más consultas o acciones reglamentarias, incluida la concesión de licencias o restricciones sobre el uso, la venta o la posesión de criptoactivos.
          Riesgos asociados con una plataforma basada en Internet. Usted reconoce que existen riesgos asociados con el uso de una Plataforma basada en Internet, incluidos, entre otros, fallas en el hardware, el software y las conexiones a Internet. Usted reconoce que MinerGate Lite no será responsable de las fallas de comunicación, las interrupciones, los errores, las distorsiones o los retrasos que pueda experimentar al usar la Plataforma, cualquiera que sea su causa.<br>
          Riesgos asociados con el Protocolo Blockchain. La Plataforma se basa en el protocolo Blockchain. Como tal, cualquier mal funcionamiento, función no intencionada, funcionamiento inesperado o ataque al protocolo Blockchain puede causar que la Plataforma funcione mal o funcione de manera inesperada o no intencionada.<br>
          Sin control sobre sus propias acciones. USTED ACEPTA INDEMNIZAR Y LIBERAR DE RESPONSABILIDAD A MINERGATE LITE CONTRA CUALQUIER RECLAMO, DEMANDA Y DAÑO, YA SEA DIRECTO, INDIRECTO, CONSECUENTE O ESPECIAL, O CUALQUIER OTRO DAÑO DE CUALQUIER TIPO, INCLUYENDO, ENTRE OTROS, PÉRDIDA DE USO, PÉRDIDA DE BENEFICIOS O PÉRDIDA DE DATOS, YA SEA EN UNA ACCIÓN, CONTRATO, AGRAVIO (INCLUYENDO, ENTRE OTROS, LA NEGLIGENCIA) O DE CUALQUIER OTRA FORMA, ORIGINADOS O RELACIONADOS DE ALGUNA FORMA CON EL USO DE NUESTRA PLATAFORMA, INCLUYENDO, ENTRE OTROS, AQUELLOS DERIVADOS DE SU ERROR PERSONAL Y MAL COMPORTAMIENTO COMO CONTRASEÑAS OLVIDADAS, RETIROS INCORRECTAMENTE CONSTRUIDOS, PÉRDIDA DE SUS ACCESOS, ETC.</p>
          </div>  
          <div class="row" style="text-align: justify;  padding-left: 15px; padding-right: 15px;" >
            <h3>INDEMNIZACIONES</h3>
          </div>
          <div class="row" style="text-align: justify;  padding-left: 15px; padding-right: 15px;" >

          <p>En la medida máxima permitida por la ley aplicable, usted acepta defender, indemnizar y eximir de responsabilidad a MinerGate Lite, su empresa matriz, afiliadas y subsidiarias y a cada uno de sus respectivos funcionarios, directores, accionistas, miembros, socios, abogados, empleados, contratistas independientes, proveedores de telecomunicaciones y agentes (colectivamente, las " Partes indemnizadas "), de y contra cualquier reclamo (incluidos los reclamos de terceros), acciones, pérdidas, responsabilidades, gastos, costos o demandas, incluidos, entre otros, los reclamos legales y honorarios contables (colectivamente, “ Pérdidas”), directa o indirectamente, como resultado de o en virtud de (i) su uso (o su uso bajo la autoridad de otra persona, incluidas, entre otras, las agencias gubernamentales), el uso indebido o la incapacidad de usar el sitio web y la plataforma o cualquiera de los información y datos contenidos en el mismo; o (ii) su incumplimiento de estos Términos.<br>
          En la medida máxima permitida por la ley aplicable, usted libera, absuelve y libera a las Partes indemnizadas de todas y cada una de las acusaciones, cargos, cargos, deudas, causas de acción, reclamos y Pérdidas, relacionadas de alguna manera con el uso de , o actividades relacionadas con el uso del sitio web y la plataforma, incluidas, entre otras, reclamaciones relacionadas con lo siguiente: negligencia, negligencia grave, interferencia intencional con un contrato o una relación comercial ventajosa, difamación, privacidad, publicidad, tergiversación, cualquier pérdida no debida al sitio web, identidades falsas, actos fraudulentos de otros, invasión de la privacidad, divulgación de información personal, transacciones fallidas, compras o funcionalidad del sitio web, falta de disponibilidad del sitio web,sus funciones y/o servicios y cualquier otra falla técnica que pueda resultar en la inaccesibilidad al sitio web, la plataforma o los servicios, o cualquier reclamo basado en la responsabilidad indirecta por daños cometidos por los usuarios que se encuentren o se realicen transacciones a través del sitio web y la plataforma, incluyendo, pero no limitado a, fraude, piratería informática, robo o uso indebido de información personal y cualquier otro tipo de delito penal.</p>
          </div>  
          <div class="row" style="text-align: justify;  padding-left: 15px; padding-right: 15px;" >
            <h3>SUSPENSIÓN Y TERMINACIÓN DE SU CUENTA</h3>
          </div>
          <div class="row" style="text-align: justify;  padding-left: 15px; padding-right: 15px;" >

          <p>En caso de incumplimiento por su parte de los Términos, o cualquier otro evento que consideremos necesario, incluidos, entre otros, una interrupción del mercado y/o un evento de fuerza mayor, podemos, a nuestro exclusivo criterio y sin responsabilidad hacia usted, con o sin previa aviso: suspender su acceso a todos o parte de nuestros Servicios; o evitar que complete cualquier acción a través de la Plataforma, incluido el retiro; o rescindir su acceso a la Plataforma, eliminar o desactivar su Cuenta y toda la información y los archivos relacionados en dicha cuenta.<br>
          Sin perjuicio de lo mencionado anteriormente, MinerGate Lite se reserva el derecho de aplicar a un usuario cualquiera de las medidas internas de Acuerdo, si:<br>
          Creemos, a nuestro exclusivo y absoluto criterio, que ha incumplido cualquier término material de estos Términos o los documentos que incorpora por referencia;<br>
          No podemos verificar o autenticar la información que nos proporciona;<br>
          Tenemos una sospecha razonable de que está utilizando directa o indirectamente nuestra plataforma en violación de la ley o regulación aplicable; <br>
          Creemos, a nuestro exclusivo y absoluto criterio, que sus acciones pueden causar responsabilidad legal para usted, nuestros usuarios o nosotros; <br>
          Estamos dirigidos como tales por una autoridad reguladora, aplicación de la ley o un tribunal de jurisdicción competente; de lo contrario, estamos obligados a hacerlo por la ley o regulación aplicable; o decidimos suspender las operaciones o descontinuar los servicios u opciones proporcionados por la plataforma o partes de los mismos.<br>
          En caso de rescisión, MinerGate Lite devolverá cualquier criptoactivo almacenado en su Cuenta y que no se le deba a la Empresa, a menos que MinerGate Lite crea que usted ha cometido fraude, negligencia u otra mala conducta.</p>
          </div>
        </div>
      </div>
    </div>
    <br /><br />
    <footer class="footer">
      <div class="container">
        <div class="row align-items-center justify-content-md-between">
          <div class="col-md-6">
            <div class="copyright">
              &copy; {{ date('Y') }} MinerGate Lite.
            </div>
          </div>
        </div>
      </div>
    </footer>
  </div>
  <script src="/web/assets/js/core/jquery.min.js" type="text/javascript"></script>
  <script src="/web/assets/js/core/popper.min.js" type="text/javascript"></script>
  <script src="/web/assets/js/core/bootstrap.min.js" type="text/javascript"></script>
  <script src="/web/assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
  <script src="/web/assets/js/plugins/bootstrap-switch.js"></script>
  <script src="/web/assets/js/plugins/nouislider.min.js" type="text/javascript"></script>
  <script src="/web/assets/js/plugins/moment.min.js"></script>
  <script src="/web/assets/js/plugins/datetimepicker.js" type="text/javascript"></script>
  <script src="/web/assets/js/plugins/bootstrap-datepicker.min.js"></script>
  <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
  <script src="/web/assets/js/argon-design-system.min.js?v=1.2.2" type="text/javascript"></script>
  <script src="https://cdn.trackjs.com/agent/v3/latest/t.js"></script>
  <script>
    window.TrackJS &&
      TrackJS.install({
        token: "ee6fab19c5a04ac1a32a645abde4613a",
        application: "argon-design-system-pro"
      });
  </script>
</body>

</html>
