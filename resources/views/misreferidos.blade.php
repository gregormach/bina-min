@extends('layouts.app')

@section('content')
<div class="content">
    @if (session('status'))
    <div class="alert alert-success" role="alert">
        {{ session('status') }}
    </div>
    @endif
    <div class="block block-rounded">
      <div class="block-header block-header-default">
        <h3 class="block-title">Mis referidos</h3>
      </div>
      <div class="block-content">
        <div class="table-responsive">
          <table class="table table-bordered table-striped table-vcenter">
            <thead>
              <tr>
                <th class="text-center">ID</th>
                <th class="text-center">Usuario</th>
                <th class="text-center">Correo</th>
                <th class="text-center">% Ganancia</th>
              </tr>
            </thead>
            <tbody>
                @foreach ($ref as $item)
                <tr>
                    <td class="text-center">
                        {{ $item->id }}
                      </td>

                    <td class="text-center">
                        {{ $item->name }}
                    </td>

                    <td class="text-center">
                      {{ $item->email }}
                    </td>

                    <td class="text-center">
                      {{ $ganancia }}
                    </td>

                  </tr>
                @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <!-- END Full Table -->
  </div>
@endsection



