<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\PermissionController;
use App\Http\Controllers\Auth\RegisterController;
use Rap2hpoutre\LaravelLogViewer\LogViewerController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'landing']);
Route::get('/nosotros', [HomeController::class, 'nosotros'])->name('nosotros');
Route::get('/recargar', [HomeController::class, 'recargar']);
Route::get('/retiro', [HomeController::class, 'retiro']);
Route::get('/atencion_al_cliente', [HomeController::class, 'atencion_al_cliente']);

Auth::routes();
Route::get('/register', [RegisterController::class, 'create'])->name('register');
Route::post('/register', [RegisterController::class, 'store']);

Route::middleware('auth')->group(function () {
    Route::get('logs', [LogViewerController::class, 'index']);
    Route::get('/home', [HomeController::class, 'index'])->name('home');
    Route::get('/plan/{id}', [HomeController::class, 'plan'])->name('plan');
    Route::post('/plan/{id}', [HomeController::class, 'store']);
    Route::get('/transactions', [HomeController::class, 'transactions'])->name('transactions');
    Route::post('/transaction/{id}', [HomeController::class, 'activeTransaction'])->name('transaction');
    
    
    Route::get('/mistransactions', [HomeController::class, 'mistransactions'])->name('mistransactions');
    Route::get('/misreferidos', [HomeController::class, 'misreferidos'])->name('misreferidos');
    Route::post('/retirar/{id}', [HomeController::class, 'retirar'])->name('retirar');

    Route::get('/complete', [HomeController::class, 'complete'])->name('complete');
    Route::post('/complete', [HomeController::class, 'completeProfile']);
    Route::get('/click', [HomeController::class, 'click'])->name('click');
    Route::get('/profile', [HomeController::class, 'profile'])->name('profile');
    Route::put('/profile/update/{id}', [HomeController::class, 'updateProfile'])->name('update.profile');

    Route::get('/events', [HomeController::class, 'events'])->name('events');

    // User
    Route::prefix('users')->group(function () {
        Route::get('/index',            [UserController::class, 'index'])->name('users.index');
        Route::get('/create',           [UserController::class, 'create'])->name('users.create');
        Route::post('/store',           [UserController::class, 'store'])->name('users.store');
        Route::get('/edit/{id}',        [UserController::class, 'edit'])->name('users.edit');
        Route::post('/update/{id}',     [UserController::class, 'update'])->name('users.update');
        Route::post('/destroy',         [UserController::class, 'destroy'])->name('users.destroy');
    });

    // Role
    Route::prefix('roles')->group(function () {
        Route::get('/index',            [RoleController::class, 'index'])->name('roles.index');
        Route::get('/create',           [RoleController::class, 'create'])->name('roles.create');
        Route::post('/store',           [RoleController::class, 'store'])->name('roles.store');
        Route::get('/edit/{id}',        [RoleController::class, 'edit'])->name('roles.edit');
        Route::post('/update/{id}',     [RoleController::class, 'update'])->name('roles.update');
        Route::post('/destroy',         [RoleController::class, 'destroy'])->name('roles.destroy');
    });

    // Permission
    Route::prefix('permissions')->group(function () {
        Route::get('/index',            [PermissionController::class, 'index'])->name('permissions.index');
        Route::get('/create',           [PermissionController::class, 'create'])->name('permissions.create');
        Route::post('/store',           [PermissionController::class, 'store'])->name('permissions.store');
        Route::get('/edit/{id}',        [PermissionController::class, 'edit'])->name('permissions.edit');
        Route::post('/update/{id}',     [PermissionController::class, 'update'])->name('permissions.update');
        Route::post('/destroy',         [PermissionController::class, 'destroy'])->name('permissions.destroy');
    });

});

